#use wml::debian::translation-check translation="cbe9cd7fab7b8b8992562b1757c8d4d429b5c67c"
<define-tag description>обновление безопасности</define-tag>
<define-tag moreinfo>
<p>Было обнаружено несколько уязвимостей в Samba, принт-, логин- и
SMB/CLI-файлового сервера для Unix.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-2031">CVE-2022-2031</a>

    <p>Люк Говард сообщает, что AD-пользователи Samba могут обходить определённые
    ограничения, связанные со сменой паролей. Пользователь, которому было
    предложено сменить свой пароль, мог эксплуатировать это для получения и
    использования тикетов для других сервисов.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-32742">CVE-2022-32742</a>

    <p>Лука Моро сообщает, что клиент SMB1 с доступом к записи долей
    может приводить к утечкам содержимого серверной памяти.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-32744">CVE-2022-32744</a>

    <p>Джозеф Саттон сообщает, что AD-пользователи Samba могут фабриковать запросы
    на смену паролей для любого пользователя, приводя к эскалации привилегий.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-32745">CVE-2022-32745</a>

    <p>Джозеф Саттон сообщает, что AD-пользователи Samba могут подвергнуть сервер
    сбою при помощи специально подготовленного LDAP-запроса на добавление или модификацию.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-32746">CVE-2022-32746</a>

    <p>Джозеф Саттон и Эндрю Бартлет сообщают, что пользователи Samba AD могут
    вызывать use-after-free у процесса сервера при помощи специально
    подготовленного LDAP-запроса на добавление или модификацию.</p></li>

</ul>

<p>В стабильном выпуске (bullseye) эти проблемы были исправлены в
версии 2:4.13.13+dfsg-1~deb11u5. Исправление для
<a href="https://security-tracker.debian.org/tracker/CVE-2022-32745">CVE-2022-32745</a>
требует обновление до ldb 2:2.2.3-2~deb11u2 для исправления дефекта.</p>

<p>Рекомендуется обновить пакеты samba.</p>

<p>С подробным статусом поддержки безопасности samba можно ознакомиться на
соответствующей странице отслеживания безопасности по адресу:
<a href="https://security-tracker.debian.org/tracker/samba">\
https://security-tracker.debian.org/tracker/samba</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5205.data"
